#!/bin/bash

function run_client {
pushd ./Buoy
python Client.py
popd
}

echo trying to activate virtualenv ...
if [[ ! -d ./venv ]]; then
	hash virtualenv 2>/dev/null || { echo >&2 "virtualenv is not installed but required.  Aborting. Use apt install virtualenv or similar"; exit 1; }
	virtualenv -p python3 venv
fi


source ./venv/bin/activate

echo installing requirements ...
pip install -r requirements.txt

helper_path=$(find venv/lib/ -name "bluepy-helper")

sudo setcap 'cap_net_raw,cap_net_admin+eip' $helper_path

run_client
