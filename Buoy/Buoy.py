
class Buoy:
    def __init__(self, id: str):
        self._id = id
    
    @property
    def id(self) -> int:
        return self._id

