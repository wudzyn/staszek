import json
import os, sys, inspect
current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(current_dir)
sys.path.insert(0, parent_dir)

import staszek_logger
import logging

logger = logging.getLogger('ReportSender')


class Reporter:
    def __init__(self, buoy_id: str):
        self._buoy_id = buoy_id
        self._report = ''
        self._beakons = []
        pass

    def add_beacons(self, beakons: list):
        self._beakons = beakons
        pass

    def get_report(self) -> dict:
        d = {'buoy_id': self._buoy_id, 'beakons': self._beakons}
        self._report = d
        return self._report

    def get_json(self) -> json:
        s = repr(self.get_report()).replace('\'', '"')
        j = json.loads(s)
        return j


class ReportSender:
    def __init__(self, server_url: str):
        self._url = server_url + '/' + 'report'.replace('//', '/').replace('//', '/')
        self._dict = None
        self._json = None
        pass

    def add_json(self, json: json):
        self._json = json

    def add_dict(self, d: dict):
        self._dict = d

    def send(self) -> tuple:
        import requests
        try:
            if self._json:
                res = requests.post(self._url, json=self._json)
            elif self._dict:
                res = requests.post(self._url, data=self._dict)
            else:
                return False, 'dupa'
        except requests.exceptions.ConnectionError as e:
            logger.error('Connection error')
            return False, repr(e)

        return res.ok, res.text
        pass



if __name__ == '__main__':
    from Buoy.BeakonInfoProvider import BeakonDetails, LiveBeacons

    lb = LiveBeacons(1)
    beakons = lb.get_beakons()
    reporter = Reporter('44')
    reporter.add_beacons(beakons)
    print(reporter.get_json())

    rs = ReportSender('https://staszek.herokuapp.com')
    rs.add_json(reporter.get_json())
    print(rs.send())
    pass


