
from ReportSender import ReportSender, Reporter
from Buoy import Buoy
from BeakonInfoProvider import LiveBeacons

import os, sys, inspect
current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(current_dir)
sys.path.insert(0, parent_dir)

import staszek_logger
import logging
logger = logging.getLogger('Client')

class Client:
    def __init__(self, global_config_file: str, buoy_config_file: str):
        """
        :param global_config_file: global config like server address
        :param buoy_config_file: buoy_id
        """

        self._config = Client._load_configs(global_config_file, buoy_config_file)
        self._bekons_of_interest = None

        self._run = True
    
    @staticmethod
    def _load_configs(gc: str, lc: str)->dict:
        import json
        with open(gc, 'rt') as f:
            g = json.load(f)
        with open(lc, 'rt') as f:
            l = json.load(f)
        g.update(l)
        return g

    def do(self):
        self._get_beakons_of_interest()
        self._buoy = Buoy.Buoy(self._config['id'])
        self._live_beakons = LiveBeacons(self._config['scan_time'])
        self._live_beakons.set_filter(self._bekons_of_interest)
        self._reporter = Reporter(self._buoy.id)
        self._report_sender = ReportSender(self._config['server'])

        import time
        while self._run:
            beakons = self._live_beakons.get_beakons()
            self._reporter.add_beacons(beakons)

            self._report_sender.add_json(self._reporter.get_json())
            ok, text = self._report_sender.send()
            logger.info(f'OK: {ok}\ntext:\n{text}')
            time.sleep(self._config['scan_time']+1)

    def _get_beakons_of_interest(self):
        import requests
        res = requests.get(self._config['server']+'/registered/'.replace('//', '/'))
        if res.ok:
            import json
            self._bekons_of_interest = json.loads(res.text)
            logger.info(self._bekons_of_interest)
        else:
            self._bekons_of_interest = None

    @staticmethod
    def get_beacons() -> list:
        '''
        which beacons I must report
        :return: [beacon_id]
        '''
        pass

    @staticmethod
    def report(reports) -> dict:
        '''
        :return: dict buoy_id: [Report]
        '''
        pass

if __name__ == '__main__':
    c = Client('global_config.json', 'buoy_config.json')
    c.do()