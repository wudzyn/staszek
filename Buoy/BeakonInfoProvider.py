
from abc import ABC
import os, sys, inspect
current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(current_dir)
sys.path.insert(0, parent_dir)

import staszek_logger
import logging
logger = logging.getLogger('BeakonInfoProvider')


class BeakonDetails(dict):
    def __init__(self, mac: str, timestamp : str, rssi: int):
        self._mac = mac
        try:
            self._timestamp = int(timestamp, 10)
        except ValueError:
            import time
            import datetime
            date_obj = datetime.datetime.strptime(timestamp, '%Y-%m-%d %H:%M:%S')
            self._timestamp = date_obj.timestamp()
        self._rssi = int(rssi, 10)

        self.__setitem__('mac', self._mac)
        self.__setitem__('timestamp', self._timestamp)
        self.__setitem__('rssi', self._rssi)


class BeakonInfoProvider(ABC):
    def __init__(self):
        self._beakons_of_intereset = None

    def get_beakons(self) -> list:
        """
        get list of known beacons
        """

    def set_filter(self, macs: list):
        self._beakons_of_intereset = macs


class LiveBeacons(BeakonInfoProvider):

    def __init__(self, scan_time: int = 2):
        super().__init__()
        self._last_update = None
        self._devices = None
        self._scan_time = scan_time
        self._refresh()


    def _need_refresh(self):
        import time
        if self._last_update and self._devices:
            return time.time() - self._last_update > self._scan_time
        else:
            return True

    def _refresh(self):
        from Beacon.get_list_of_currently_visible_beacons import get_devices
        import time
        if self._need_refresh():
            self._devices = get_devices(self._scan_time)
            self._last_update = time.time()
        #{'timestamp': '2020-11-24 13:19:53', 'beacon_id': '59:b4:f0:8d:05:00', 'RSSI': '-82'}
            self._beakons = list(map(lambda x: BeakonDetails(x['beacon_id'], x['timestamp'], x['RSSI']), self._devices))



    def get_beakons(self) -> list:
        self._refresh()
        if self._beakons_of_intereset:
            beakons = list(filter(lambda x: x['mac'] in self._beakons_of_intereset, self._beakons))
        else:
            beakons = self._beakons

        logger.info(repr(beakons).replace('},', '}\n'))
        return beakons


if __name__ == '__main__':
    bd = BeakonDetails('ab:cd', '2020-11-24 13:19:53', '34')
    print(bd)

    bp = LiveBeacons()
    print(bp.get_beakons())