from Data import Report
from PIL import Image, ImageDraw
import json
import rootpath
import os
import numpy as np
import math

ROOTPATH = rootpath.detect(os.path.dirname(__file__))
ASSERTS_DIRPATH = os.path.join(ROOTPATH, 'Server', 'assets')
MAPS_DIRPATH = os.path.join(ASSERTS_DIRPATH, 'maps')
BUOYS_DEFINITION_FILEPATH = os.path.join(ASSERTS_DIRPATH, 'buoys.json')


class Raport:
    def __init__(self, timestamp: int, rssi: int):
        self.timestamp = timestamp
        self.rssi = rssi


fake_raport = {
    'BUOY_1': [(1606211131, -55), (1606211130, -26), (1606211002, -31)],
    'BUOY_2': [(1606211131, -30)],
    'BUOY_3': [(1606211131, -88), (1606211130, -41), (1606211002, -28)]
}

fake_raport_0 = {}
fake_raport_1 = {'BUOY_2': [(1606211131, -30)]}
fake_raport_2 = {
    'BUOY_2': [(1606211131, -30)],
    'BUOY_3': [(1606211131, -88), (1606211130, -41), (1606211002, -28)]}
fake_raport_2_equally_strong = {
    'BUOY_2': [(1606211131, -30)],
    'BUOY_3': [(1606211131, -30)]}
fake_raport_3_equally_strong = {
    'BUOY_1': [(1606211131, -30)],
    'BUOY_2': [(1606211131, -30)],
    'BUOY_3': [(1606211131, -30)]
}


class Point:
    def __init__(self, x: int, y: int):
        self.x = x
        self.y = y


class MightyAlgorithm:
    """
    TODO: 1. decide how to handle rapports from buoys which are on different maps
    """

    def __init__(self, raports: dict):
        self.raports = MightyAlgorithm._parse_raport(raports)
        self.buoy_definition = MightyAlgorithm._load_json(BUOYS_DEFINITION_FILEPATH)

    def _draw_info(self, drawer: ImageDraw, data, position: Point, color: str = 'green'):
        def _stringify_data(data):
            text = ''
            for raport in data:
                try:
                    for item in data[raport]:
                        text += f'{raport}: {item.rssi}\n'
                except TypeError:
                    text += f'{raport}: {data[raport].rssi}\n'
            return text

        drawer.text((position.x, position.y), _stringify_data(data), fill=color)

    def _prepare_drawer(self):
        map = self.buoy_definition[list(self.raports.keys())[0]]['map_id']
        img = Image.open(os.path.join(MAPS_DIRPATH, map))
        return map, img, ImageDraw.Draw(img)

    def draw(self, img_name: str = 'tu_jestem.png', debug=False) -> str:
        '''
        :return: path to image with position of requested device on map
        '''
        if not self.raports:
            return None

        # FIXME: currently works with assumption that all buoys use the same map
        map, img, drawer = self._prepare_drawer()
        valid = self._get_valid()
        if debug:
            self._draw_info(drawer, self.raports, position=Point(5, 595), color='green')
            self._draw_info(drawer, valid, Point(300, 30), color='blue')
            self._draw_buoys(map, img)

        if len(valid) == 0:
            raise ValueError('No valid data found')
        else:
            def _get_3_strongest(data: dict):
                # temp: first 3 data, all are within +- 20 (or +-10) range so it should not matter that much
                # if less than 3 data available - will return 2
                return {k: data[k] for k in list(data)[:3]}

            valid = _get_3_strongest(valid)

            x = 0
            y = 0
            for v in valid:
                name = v
                buoy = self.buoy_definition[name]
                x = buoy['x'] + x
                y = buoy['y'] + y

            center = Point(x / len(valid), y / len(valid))
            radius = MightyAlgorithm._to_px(MightyAlgorithm._get_distance_from_buoy(valid[name].rssi, 2), 0.765957446808511)
            self._draw_circle(drawer, center, radius)
        img.save(path := os.path.join(ASSERTS_DIRPATH, 'draws', img_name))
        return path

    def _get_circumference(self, point: Point, radius: int) -> list:
        def rotate(x, y, r):
            rx = (x * math.cos(r)) - (y * math.sin(r))
            ry = (y * math.cos(r)) + (x * math.sin(r))
            return (rx, ry)

        def point_ring(center: Point, num_points, radius):
            arc = (2 * math.pi) / num_points  # what is the angle between two of the points
            points = []
            for p in range(num_points):
                (px, py) = rotate(0, radius, arc * p)
                px += center.x
                py += center.y
                points.append((round(px), round(py)))
            return points

        return point_ring(point, 10000, radius)

    @staticmethod
    def _load_json(filepath: str):
        with open(filepath) as file:
            return json.load(file)

    def _find_common_points(self):
        common_points = []

        for definition in self.buoy_definition:
            if definition in self.raports:
                defy = self.buoy_definition.get(definition)
                center = Point(defy['x'], defy['y'])
                radius = MightyAlgorithm._to_px(
                    MightyAlgorithm._get_distance_from_buoy(self.raports[definition].rssi, 2))
                points = self._get_circumference(center, radius)
                common_points = MightyAlgorithm._get_intersection(common_points, points)

        return common_points

    @staticmethod
    def _get_intersection(listA: list, listB: list):
        if not listA and not listB:
            raise ValueError('empty lists')
        elif not listA:
            return listB
        elif not listB:
            return listA
        else:
            return list(set(listA).intersection(listB))

    def _draw_buoys(self, mapka, img: Image):
        # TODO: draw only buoys on valid map
        with Image.open(os.path.join(ASSERTS_DIRPATH, 'staszek_icon.png')) as staszek:
            for definition in self.buoy_definition:
                buoy = self.buoy_definition.get(definition)
                if buoy.get('map_id') == mapka:
                    point = Point(buoy.get('x'), buoy.get('y'))
                    img.paste(staszek, (point.x, point.y))

    def _draw_circle(self, drawer: object, center: Point, radius: int):
        drawer.ellipse((center.x - radius, center.y - radius, center.x + radius, center.y + radius),
                       fill='lightcoral', outline='red')

    @staticmethod
    def _parse_raport(raport: dict):
        for buoy in raport:
            result = []
            for data in raport[buoy]:
                result.append(Raport(*data))

            raport.update({buoy: result})
        return raport

    def _get_newest(self):
        for buoy in self.raports:
            timestamp = 0
            newest = None
            for raport in self.raports[buoy]:
                if raport.timestamp > timestamp:
                    timestamp = raport.timestamp
                    newest = raport
            self.raports[buoy] = newest

    def _get_valid(self):
        self._get_newest()

        if len(self.raports) < 2:
            return self.raports
        else:
            return self._get_strongest_raport()

    def _get_strongest_raport(self):
        strongest = -200
        for raport in self.raports:
            if self.raports[raport].rssi > strongest:
                strongest = self.raports[raport].rssi
        raport_dict = {}
        for raport in self.raports:
            if self.raports[raport].rssi in range(int(1.2 * strongest), int(0.8 * strongest)):
                raport_dict[raport] = self.raports[raport]
        return raport_dict

    @staticmethod
    def _get_distance_from_buoy(rssi: int, strength: int, power: int = -69) -> int:
        """return distance in cm
        1cm ~= 0.83px
        """
        if strength not in range(2, 5):
            raise ValueError(f'Strength value should be in range of 2-4')
        if rssi not in range(-100, -25):
            raise ValueError(f'RSSI from outside of supported range')
        return 10 ** ((power - rssi) / 10 ** strength) * 100

    @staticmethod
    def _to_px(distance: int, factor: int = 0.83):
        # TODO scaling factor mapping for each map
        """ each map should have definition of N(px)/1cm assigned. This factor should be used here
        i.e for dol.png 0.83px == 1cm"""
        return round(factor * distance)


class TestMe:
    def test_parsing(self):
        alg = MightyAlgorithm(fake_raport)
        print(alg.raports)
        alg._get_newest()
        print(alg.raports)
        for buoy in ['BUOY_1', 'BUOY_2', 'BUOY_3']:
            print(alg.raports[buoy].timestamp)
            print(alg.raports[buoy].rssi)
            distance = MightyAlgorithm._get_distance_from_buoy(alg.raports[buoy].rssi, 2)
            print(distance)
            print(MightyAlgorithm._to_px(distance))

    def test_draw_buoys(self):
        alg = MightyAlgorithm(fake_raport)
        alg._get_newest()
        alg._draw_buoys(os.path.join(MAPS_DIRPATH, 'dol.png'), BUOYS_DEFINITION_FILEPATH)

    def test_draw(self):
        alg = MightyAlgorithm(fake_raport_1)
        alg.draw()

    def test_valid(self):
        for idx, raport in enumerate([fake_raport_0, fake_raport_1, fake_raport_2, fake_raport_2_equally_strong,
                                      fake_raport_3_equally_strong]):
            print(name := f'raport_{idx}')
            alg = MightyAlgorithm(raport)
            alg.draw(f'{name}.png', True)
