import rootpath
rootpath.append()

from flask import Flask, request, render_template, redirect, url_for, g, jsonify, send_file
from TheAlgorithm.MightyAlgorithm import MightyAlgorithm
from collections import defaultdict
from markupsafe import escape
import json
import time
import sqlite3
import os

import logging

logger = logging.getLogger('myServer')
logger.setLevel(logging.DEBUG)

fh = logging.FileHandler('server.log')
fh.setLevel(logging.CRITICAL)

ch = logging.StreamHandler()
ch.setLevel(logging.CRITICAL)

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
ch.setFormatter(formatter)

logger.addHandler(fh)
logger.addHandler(ch)

class ServerError(Exception):
    def __init__(self, message):
        super().__init__()
        self.message = message
        print(f'ERROR  - {message}')

DEBUG=False
DATABASE = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'database.db')
DB_SCHEMA = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'db_schema')
BUOYS = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'assets', 'buoys.json')
SAMPLE_LIFETIME = 3*60

with open(BUOYS, 'r') as b:
    KNOWN_BUOYS = list(json.load(b).keys())


def has_beacon_with_id(db, beacon_id):
    c = db.cursor()
    c.execute("SELECT _id FROM beacons WHERE _id=?",  (beacon_id,))
    return bool(len(c.fetchall()))


def insert_beacon(db, id, name):
    c = db.cursor()
    c.execute("SELECT _id FROM beacons WHERE _id=?",  (id,))
    if len(c.fetchall()):
        return
    c.execute("INSERT INTO beacons VALUES (?, ?)", (id, name))
    db.commit()


def update_name(db, id, new_name):
    c = db.cursor()
    c.execute('UPDATE beacons SET _name=? WHERE _id=?', (new_name, id))
    db.commit()


def remove_beacon(db, id):
    c = db.cursor()
    c.execute('DELETE FROM beacons WHERE _id=?', (id,))
    db.commit()


def get_ids_and_names(db):
    c = db.cursor()
    c.execute("SELECT * FROM beacons")
    result = []
    for entry in c.fetchall():
        result.append((entry[0], entry[1]))
    return result


def store_value_for(db, beacon_id, reporter_id, timestamp, value):
    c = db.cursor()
    if has_beacon_with_id(db, beacon_id):
        c.execute("INSERT INTO samples (_timestamp, _value, _beacon, _buoy) VALUES (?, ?, ?, ?)", (timestamp, value, beacon_id, reporter_id))
    db.commit()


def clean_old_samples(db):
    c = db.cursor()
    c.execute('DELETE FROM samples WHERE _timestamp<?', (time.time() - SAMPLE_LIFETIME,))
    db.commit()


def get_beacon_info(db, bekon_id):
    c = db.cursor()
    c.execute("select _timestamp, _value, _buoy from samples where _beacon = ?", (bekon_id,))
    result = defaultdict(list)
    for ts, v, b in c.fetchall():
        result[b].append([ts, v])
    return result


app = Flask(__name__)


def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db_is_new = not os.path.exists(DATABASE)
        db = g._database = sqlite3.connect(DATABASE)
        if db_is_new:
            with open(DB_SCHEMA, 'rt') as f:
                schema = f.read()
                db.executescript(schema)
    return db


@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()


@app.route('/')
def main_page():
    return render_template('main.html', bekons=get_ids_and_names(get_db()))


@app.route('/registered/')
def get_registered_bekons():
    bekons = [id for id, name in get_ids_and_names(get_db())]
    return jsonify(bekons)


@app.route('/report/', methods=['POST'])
def store_report():
    try:
        clean_old_samples(get_db())
        payload = request.get_json(silent=True, force=True)
        if payload is not None and payload['buoy_id'] in KNOWN_BUOYS:
            print(f'store_report: receive payload {request.form}')
            for sample in payload['beakons']:
                store_value_for(db=get_db(),
                                beacon_id=sample['mac'],
                                reporter_id=payload['buoy_id'],
                                timestamp=sample['timestamp'],
                                value=sample['rssi'])
        else:
            print(f'WARNING: store_report: invalid payload {request.form}')
    except:
        print(f'WARNING: store_report: invalid payload {request.form}')
    return ''


@app.route('/show/<bekon_id>')
def show(bekon_id):
    id = '%s' % escape(bekon_id)
    if DEBUG:
        bekon_info = get_beacon_info(get_db(), id)
        result = []
        for buoy, reports in bekon_info.items():
            tmp=[]
            tmp.append(f'buoy={buoy}')
            tmp = tmp + [f'(time:{r[0]} rssi:{r[1]})' for r in reports]
            result.append(tmp)
        return render_template('show.html', values=result)

    bekon_info = get_beacon_info(get_db(), id)
    algo = MightyAlgorithm(bekon_info)
    if result := algo.draw(debug=True):
        return send_file(result)
    return render_template('show.html', values=['Położenie nieznane :('])


@app.route('/register/', methods=['POST', 'GET'])
def register_new():
    if request.method == 'GET':
        return render_template('register.html')
    if request.method == 'POST':
        payload = request.get_json(silent=True, force=True)
        form = request.form
        logger.info(f'payload: {payload}, type: {type(payload)} form:{form}')
        if payload:
            logger.info(f'payload: {payload}')
            for p in payload:
                logger.info(f'p: {p}')
                insert_beacon(get_db(), p['id'], p['name'])
            return f'processed payload\n{payload}'
        else:
            logger.info(f'form: {form}')
            id = '%s' % escape(request.form['id'])
            name = '%s' % escape(request.form['name'])
            insert_beacon(get_db(), id, name)
            return redirect(url_for('main_page'))

@app.route('/delete/', methods=['POST', 'GET'])
def delete():
    if request.method == 'GET':
        return render_template('delete.html', ids=[id for id, name in get_ids_and_names(get_db())])
    id = '%s' % escape(request.form['to_delete'])
    remove_beacon(get_db(), id)
    return redirect(url_for('main_page'))


@app.route('/edit/', methods=['POST', 'GET'])
def edit():
    if request.method == 'GET':
        return render_template('edit.html', ids_names=get_ids_and_names(get_db()))

    for k, v in request.form.items():
        id = '%s' % escape(k)
        name = '%s' % escape(v)
        update_name(get_db(), id, name)
    return redirect(url_for('main_page'))

def run():
    from waitress import serve
    serve(app)

if __name__ == '__main__':
    from waitress import serve
    serve(app, host="0.0.0.0", port=5050)

