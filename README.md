# Staszek

To install bluepy:
sudo apt-get install libglib2.0-dev
python3 -m pip install bluepy

To run sample scanner:
python3 -m sample_scanner.py


## report_object
```
{
    "timestamp": linux_timestamp,
    "mac": mac,
    "rssi": rssi
}
```
## bouy_report
```
{
    "buoy_id": id,
    "beakons" :
    [
        report_object
    ]
}
```

interface:
```
get_list_of_currently_visible_beakons()-> list
get_buoy_id()->int
```

## reporting to server
### endpoint 
`report` - POST - buoy report as json


