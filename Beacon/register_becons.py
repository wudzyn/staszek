known = '[{"id": "fc:58:fa:59:64:67", "name": "michal"}, ' \
        '{"id": "fc:58:fa:5a:11:70", "name": "bekon_2"}, ' \
        '{"id": "fc:58:fa:58:78:78", "name": "bekon_1"},' \
        '{"id": "fc:58:fa:57:ea:9b", "name": "bekon_4"},' \
        '{"id": "fc:58:fa:5a:08:88", "name": "bekon_5"}]'


def register(beakons: str, server_url: str):
    import json
    import requests

    b = json.loads(beakons)
    ret = requests.post(f'{server_url}/register/', data=beakons)
    print(ret.text)
    pass


if __name__ == '__main__':
    register(known, 'https://staszek.herokuapp.com')
    #register(known, 'http://0.0.0.0:5000')