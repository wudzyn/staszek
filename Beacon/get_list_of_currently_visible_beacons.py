from bluepy.btle import Scanner, DefaultDelegate
import datetime


class ScanDelegate(DefaultDelegate):
    def __init__(self):
        DefaultDelegate.__init__(self)


def get_devices(scan_time : float = 10.0):
    scanner = Scanner().withDelegate(ScanDelegate())
    ct = datetime.datetime.now().replace(microsecond=0)
    devices = scanner.scan(scan_time, passive=True)
    discovered_devices = []
    for dev in devices:
        discovered_devices.append({'timestamp':f'{ct}', 'beacon_id':f'{dev.addr}', 'RSSI':f'{dev.rssi}'})
    return discovered_devices

if __name__ == "__main__":
    print(get_devices(10))